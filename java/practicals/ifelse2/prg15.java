class Ifelsedemo{
	public static void main(String[]args){
		int num=20;
		if(num%3==1 && num%7==1){
			System.out.println("20 is neither divisible by 3 nor by 7");
		}else if (num%3==0 && num%7==0){
			System.out.println("20 is divisible by 3 and 7");
		}
	}
}
