import java.util.*;
class Conversion{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the array of chracters: ");
		String input=sc.nextLine();

		char[] charArray=input.toCharArray();

		for(int i=0;i<charArray.length;i++){
			if(Character.isLowerCase(charArray[i])){
				charArray[i]=Character.toUpperCase(charArray[i]);
			}
		}
		String output=new String(charArray);
		System.out.println("Converted array of chracters: " + output);
	}
}

