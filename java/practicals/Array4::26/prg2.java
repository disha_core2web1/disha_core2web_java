import java.util.*;
class Difference{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		
		System.out.print("Enter the size of the array: ");
		int size=sc.nextInt();

		int[] arr=new int[size];

		System.out.println("Enter the elements of the array:");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int min=arr[0];
		int max=arr[0];

		for(int i=1;i<size;i++){
			if(arr[i]<min){
			min=arr[i];
		}

		if(arr[i]>max){
			max=arr[i];
		}
		}
		int difference=max-min;
		System.out.println("The Difference between minimum and maximum elements: " + difference);
	}
}

