import java.util.*;
class Reverse{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
	
		System.out.print("Enter the size of the array: ");
		int size=sc.nextInt();

		int[] array=new int[size];

		System.out.println("Enter the elements of the array: ");
		for(int i=0;i<size;i++){
			array[i]=sc.nextInt();
		}
		reverseArray(array);

		System.out.println("Reversed array: ");
		for(int num : array ){
			System.out.print(num + " " );
		}
		sc.close();
	}
	public static void reverseArray(int[] array){
		int start=0;
		int end=array.length -1;

		while(start<end){
			int temp=array[start];
			array[start]=array[end];
			array[end]=temp;

			start++;
			end--;
		}
	}
}

	
