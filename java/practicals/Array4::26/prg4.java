import java.util.*;
class Occurance{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);

		System.out.print("Enter the size of the array: ");
		int size=sc.nextInt();

		int[] numbers=new int[size];
		System.out.println("Enter the elements of the array:");
		for(int i=0;i<size;i++){
			numbers[i]=sc.nextInt();
		}

		System.out.print("Enter the number to check occurance: " );
		int targetNumber=sc.nextInt();

		int count=countOccurances(numbers, targetNumber);

		if(count>2){
			System.out.println(targetNumber + " occurs more than 2 times.");
		} else {
			System.out.println(targetNumber + "does not occur more than 2 times.");
		}
		sc.close();
	}
	public static int countOccurances(int[] numbers,int target){
		int count=0;
		for(int num : numbers){
			if(num==target){
				count++;
			}
		}
		return count;
	}
}
		 

