import java.util.Scanner;
class AverageDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of array");
		int size=sc.nextInt();
		int[] arr=new int[size];

		System.out.print("Enter the elements of the array:");
		for(int i=0; i<size ;i++){
			arr[i]=sc.nextInt();
		}
		double sum=0;
		for(int num : arr){
			sum+=num;
		}
		double average = sum/size;
		System.out.println("Average of the array elements: " + average);
	}
}


