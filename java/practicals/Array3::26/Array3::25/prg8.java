class Composite{
	public static void main(String[]args){
		int[] numbers={4,5,7,9,10};

		System.out.println("Composite numbers in the array: ");
		for(int number:numbers){
			if(isComposite(number)){
				System.out.println(number);
			}
		}
	}

		public static boolean isComposite(int num){
			if(num<=1){
				return false;
			}
			for(int i=2; i<=Math.sqrt(num);i++){
				if(num %i==0){
					return true;
				}
			}
			return false;
		}
}

