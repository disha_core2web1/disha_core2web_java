import java.util.*;
class FirstOccur{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size: ");
		int size=sc.nextInt();
		System.out.println("Enter the elements: ");
		int arr[]=new int[size];
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter Specific number: ");
		int num=sc.nextInt();
		int count=0;
		for(int i=0;i<size;i++){
			if(arr[i]==num){
				count++;
			}
		}
		System.out.println("number " + num + " is occured " + count + " times in an array" );
	}
}


