class Prime{
	public static void main(String[]args){
		int[] numbers={5,7,9,11,15,19,90};

		System.out.println("Prime numbers in an array: ");
		for(int number : numbers){
			if (isPrime(number)){
				System.out.println(number);
			}
		}
	}
	public static boolean isPrime(int num){
		if(num<=1){
			return false;
		}
		for(int i=2;i<=Math.sqrt(num);i++){
			if(num%i==0){
				return false;
			}
		}
		return true;
	}
}

