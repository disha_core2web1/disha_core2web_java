class ProductPrime{
	public static void main(String[]args){
		int[] numbers={1,4,5,6,11,9,10};
		long product= getProductOfPrimes(numbers);

		System.out.println("Product of prime numbers in the array: " + product);
	}
	public static long getProductOfPrimes(int[] numbers){
		long product=1;
		for(int number: numbers){
			if (isPrime(number)){
				product *=number;
			}
		}
		return product;
	}
	public static boolean isPrime(int num){
		if(num<=1){
			return false;
		}
		for(int i=2; i<=Math.sqrt(num);i++){
			if(num%i==0){
				return false;
			}
		}
		return true;
	}
}


