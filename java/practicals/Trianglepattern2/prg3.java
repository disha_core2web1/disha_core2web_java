import java.io.*;
class Demo{
	public static void main(String[]args)throws IOException{
		BufferedReader ip=new BufferedReader (new InputStreamReader(System.in));
		System.out.print("Enter the number ");
		int row= Integer.parseInt(ip.readLine());
		int ch=64+row;
		for(int i=1;i<=row;i++){
			char Ch='D';
			for(int j=1;j<=i;j++){
				System.out.print((char)ch-- + " " );
			}
			System.out.println();
			ch='D';
		}
	}
}


