import java.io.*;
class demo{
	public static void main(String[]args)throws IOException{
		BufferedReader ip=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the Number of Rows: ");
		int rows=Integer.parseInt(ip.readLine());
		int num=1;
		char ch='c';
		System.out.println("Enter Pattern: ");
	
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(i + " ");
				}else{
					System.out.print(ch + " " );
					ch+=2;
				}
		}System.out.println();
	}
}
}

