import java.util.*;
class Array5{
	public static void  main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of the array: ");
		int size=sc.nextInt();
		int[] arr=new int[size];
		System.out.println("Enter the elements of the array: ");
		for(int i=0;i<size;i++){
			arr[i] =sc.nextInt();
		}
		System.out.print("Output: ");
		for(int i=0;i<size;i++){
			int count=0;
			int num=arr[i];
			while(num !=0){
				num/=10;

				count++;
			}
			System.out.print(count);
			if(i<size-1){
				System.out.print(" , ");
			}
		}
	}
}
