import java.util.*;
class Array1{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of the array: ");
		int size=sc.nextInt();

		int[] arr=new int[size];
		System.out.println("Enter the elements of the arrazy: ");
		for(int i=0;i<size;i++){
			arr[i] =sc.nextInt();
		}
		boolean isAscending=true;
		for(int i=1;i<size;i++){
			if(arr[i]<arr[i-1]){
				isAscending=false;
				break;
			}
		}
		if(isAscending){
			System.out.println("The given array is in ascending Order. ");
		}else{
			System.out.print("The given array is not in ascending Order." );
		}
	}
}
