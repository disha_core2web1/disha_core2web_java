import java.util.*;
class EvenNum{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size");
		int size=sc.nextInt();
		int array[] =new int[size];

		System.out.println("Enter " + size + " elements : ");
		for(int i=0;i<size;i++){
			array[i]=sc.nextInt();
			int countEven=0;
			System.out.println("Even Number : ");

			for(int i=0;i<size;i++){
				if(array[i] % 2==0){
					countEven++;
					System.out.println(array[i] + " " );
				}
			}
		}
	}
}
