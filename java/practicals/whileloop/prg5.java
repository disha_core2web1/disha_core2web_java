class OddEven{
	public static void main(String[]args){
		int num=214367689;
		int countEven=1;
		int countOdd=1;
		while(num>0){
			num =num/10;
			if(num%2==0){
				countEven++;
			}else{
				countOdd++;
			}
		}
		System.out.println("Even Count = "+countEven);
		System.out.println("Odd Count = " +countOdd);
	}
}

