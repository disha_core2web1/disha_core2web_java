import java.util.Scanner;
class Product{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of array: " );
		int size=sc.nextInt();
		int[]arr=new int[size];
		System.out.println("Enter the elements of array: ");
		for(int i=0;i<size; i++){
			arr[i]=sc.nextInt();
		}
		int product=1;
		for(int i=1;i<size;i+=2){
			product*=arr[i];
		}
		System.out.println("Product of Odd Indexed elements is : " + product);
	}
}



