import java.util.Scanner;
class Sum{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of array: ");
		int size=sc.nextInt();

		int[] arr=new int[size];
		System.out.println("Enter the elements of array: ");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		   int sum=0;
		   for(int i=1;i<size;i+=2){
			   sum+=arr[i];
		   }
		   System.out.println("Sum of Odd Indexed elements is : "+sum );
	}
}

		
