import java.util.Scanner;
class PrintArray{
	public static void main(String[]args){
	Scanner sc=new Scanner(System.in);
	System.out.print("Enter the size of the array: " );
	int size=sc.nextInt();
	int[] array=new int[size];

	System.out.println("Enter the elements of array: ");
	for(int i=0;i<size;i++){
		System.out.print("Enter element at index" + i + " : " );
		array[i]=sc.nextInt();
	}
	System.out.println("Array : " );
	if(size%2==0){
		System.out.println("Alternate elements : " );
		for(int i=0;i<size;i+=2)
			System.out.print(array[i] + " " );
	}else{
		System.out.println("Whole Array: ");
		for(int i=0;i<size;i++){
			System.out.print(array[i] + " " );
		}
	}
	}
}



