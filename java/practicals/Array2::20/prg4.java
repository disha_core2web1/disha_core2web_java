import java.util.*;
class SearchNum{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size: ");
		int size=sc.nextInt();

		int[] array=new int[size];

		System.out.println("Enter the elements: ");
		for(int i=0;i<size;i++){
			array[i]=sc.nextInt();
		}
		System.out.println("Enter the number to search in array: ");
		int searchNumber =sc.nextInt();

		boolean isFound=false;
		for(int i=0;i<array.length;i++){
			if(array[i]==searchNumber){
				System.out.println(searchNumber + " found at Index " + i);
				isFound= true;
				break;
			}
		}
	}
}
