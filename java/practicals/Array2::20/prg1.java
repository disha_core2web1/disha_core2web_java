import java.util.Scanner;
class Even{
         public static void main(String[]args){
	Scanner sc=new Scanner(System.in);
	
	System.out.print("Enter the size of the array: ");
	int size=sc.nextInt();
	
	int[] array=new int[size];
	
	System.out.println("Enter the elements of the array:");
	for(int i=0;i<size;i++){
		array[i]=sc.nextInt();
	}
	 
	System.out.println("Even numbers in the array: ");
	int evenCount=0;
        for(int num:array){
        if(num%2==0){
                System.out.print(num+ " " );
                evenCount++;
       }
       }
       System.out.println("\n Total even numbers: " + evenCount);
}
}




