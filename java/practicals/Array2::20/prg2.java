import java.util.Scanner;
class Divisible{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the size of the array: ");
		int size=sc.nextInt();

		int[] arr=new int[size];
		System.out.println("Enter the elements of the array:");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}

		int sum=0;
		for(int i=0;i<size;i++){
			if(arr[i]%3==0){
					
				sum += arr[i];
			}
		}
		System.out.println("Sum of elements divisible by 3: "+ sum);
	}
}

