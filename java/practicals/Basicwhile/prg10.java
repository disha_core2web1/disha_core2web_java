class Demo{
	public static void main(String[]args){
		for(char c='A'; c <='Z'; c++){
			if(!isVowel(c)){
				System.out.println(c + " ");
			}
		}
	}
	public static boolean  isVowel(char c){
		c=Character.toUpperCase(c);
		return c=='A'|| c=='E' || c=='I' || c=='O' ||c =='U';
	}
}
