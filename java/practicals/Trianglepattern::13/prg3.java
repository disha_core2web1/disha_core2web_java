import java.io.*;
class pattern{
	public static void main(String[]args)throws IOException{
		BufferedReader ip= new BufferedReader (new InputStreamReader(System.in));
		System.out.print("Enter number of rows: ");
		int rows=Integer.parseInt(ip.readLine());
		int num=20;
		
		for(int i=rows;i>0;i--){
			for(int j=0;j<i;j++){
				System.out.print(num + " ");
				num-=2;
			}
			System.out.println();
		}
	}
}

