class Division{
	public static void main(String[]args){
		int num=436780521;
		int i=num;
		System.out.print("digits divisible by 2 or 3 are: ");
		while(i!=0){
			int rm=i%10;
			if(rm%3==0 || rm%2==0){
				System.out.print(rm + " ");
			}
			i=i/10;
		}
		System.out.println();
	}
}
